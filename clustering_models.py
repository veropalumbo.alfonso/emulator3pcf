"""
python module for calculating integrals with 2 Bessel / spherical Bessel functions.

by Xiao Fang
Feb 22, 2020
"""
from scipy.fft import rfft2, irfft2
from scipy.interpolate import interp1d
from scipy.special import gamma as gammafunc
import numpy as np
from scipy.integrate import quad, romb

def func1(xx, b, a, alpha=1):  
    x = alpha*xx
    return x**(3-b) * ( a * np.sin(a*np.log(x)) - (b-3) * np.cos(a*np.log(x))) / (a**2+(b-3)**2)

def func2(xx, b, a, alpha=1):  
    x = alpha*xx
    return x**(3-b) * ( (b-3) * np.sin(a*np.log(x)) + a * np.cos(a*np.log(x))) / (a**2+(b-3)**2)

class two_sph_bessel(object):

    def __init__(self, x1, x2, fx1x2, nu1=1.01, nu2=1.01, N_extrap_low=0, N_extrap_high=0, c_window_width=0.25, N_pad=0):

        self.x1_origin = self.x1 = x1 # x is logarithmically spaced
        self.x2_origin = self.x2 = x2

        # self.lnx = np.log(x)
        self.dlnx1 = np.log(x1[1]/x1[0])
        self.dlnx2 = np.log(x2[1]/x2[0])
        self.fx1x2_origin= self.fx1x2 =fx1x2 # f(x1,x2) array
        self.nu1 = nu1
        self.nu2 = nu2
        self.N_extrap_low = N_extrap_low
        self.N_extrap_high = N_extrap_high
        self.c_window_width = c_window_width

        self.N1 = self.x1.size
        self.N2 = self.x2.size
        if((self.N1+N_extrap_low + N_extrap_high)%2==1 or (self.N2+N_extrap_low + N_extrap_high)%2==1): # Make sure the array sizes are even
            print("Error: array sizes have to be even!")
            exit()

        # extrapolate x and f(x) linearly in log(x), and log(f(x))
        if(N_extrap_low or N_extrap_high):
            self.x1 = log_extrap(x1, N_extrap_low, N_extrap_high)
            self.x2 = log_extrap(x2, N_extrap_low, N_extrap_high)
            self.fx1x2 = bilinear_extra_P(fx1x2, N_extrap_low, N_extrap_high)
            self.N1 += N_extrap_low + N_extrap_high
            self.N2 += N_extrap_low + N_extrap_high

        # zero-padding
        self.N_pad = N_pad
        if(N_pad):
            pad = np.zeros(N_pad)
            self.x1 = log_extrap(self.x1, N_pad, N_pad)
            self.x2 = log_extrap(self.x2, N_pad, N_pad)
            self.N1 += 2*N_pad
            self.N2 += 2*N_pad
            zeros = np.zeros((self.N1, self.N2))
            zeros[N_pad:-N_pad, N_pad:-N_pad] = self.fx1x2
            self.fx1x2 = zeros
            self.N_extrap_high += N_pad
            self.N_extrap_low += N_pad

        self.m, self.n, self.c_mn = self.get_c_mn()
        self.eta_m = 2*np.pi/self.dlnx1 / float(self.N1) * self.m
        self.eta_n = 2*np.pi/self.dlnx2 / float(self.N2) * self.n

        self.x10 = self.x1[0]
        self.x20 = self.x2[0]
        self.z1 = self.nu1 + 1j*self.eta_m
        self.z2 = self.nu2 + 1j*self.eta_n

        self.y1 = 1. / self.x1[::-1]
        self.y2 = 1. / self.x2[::-1]
        self.y10 = self.y1[0]
        self.y20 = self.y2[0]
        
        self.g1 = None
        self.g2 = None
        self.mat1 = None
        self.mat2 = None

    def get_c_mn(self):
        """
        return m and c_mn
        c_mn: the smoothed 2D-FFT coefficients of "biased" input function f(x):
        f_b = f(x_1,x_2) / x_1^\nu_1 / x_2^\nu_2

        number of x1, x2 values should be even
        c_window_width: the fraction of any row/column c_mn elements that are smoothed.
        """
        f_b=((self.fx1x2*self.x2**(-self.nu2)).T*self.x1**(-self.nu1)).T
        c_mn=rfft2(f_b)

        m = np.arange(-self.N1//2,self.N1//2+1)
        n = np.arange(-self.N2//2,self.N2//2+1)

        c_mn1 = c_mn[:self.N1//2+1,:]
        c_mn = np.vstack((c_mn[self.N1//2:,:], c_mn1[:,:]))

        c_mn_left = np.conj(np.flip(np.flip(c_mn,0),1))
        c_mn = np.hstack((c_mn_left[:,:-1], c_mn))

        c_window_array1 = c_window(m, int(self.c_window_width*self.N1//2.) )
        c_window_array2 = c_window(n, int(self.c_window_width*self.N2//2.) )
        c_mn_filter = ((c_mn*c_window_array2).T*c_window_array1).T
        return m, n, c_mn
    
    def set_gg(self, ell1, ell2):
        
        self.g1 = g_l(ell1,self.z1)*(self.x20)**(-1j*self.eta_m)
        self.g2 = g_l(ell2,self.z2)*(self.x10)**(-1j*self.eta_n)
        self.mat2 = self.c_mn*(self.x20)**(-1j*self.eta_n) * self.g2
        self.mat1 = self.c_mn*(self.x10)**(-1j*self.eta_m) * self.g1
        
    def two_sph_bessel_r1r2(self, r12, r13, alpha=1):
        """
        Calculate F(y_1,y_2) = \int_0^\infty dx_1 / x_1 \int_0^\infty dx_2 / x_2 * f(x_1,x_2) * j_{\ell_1}(x_1y_1) * j_{\ell_2}(x_2y_2),
        where j_\ell is the spherical Bessel func of order ell.
        array y is set as y[:] = 1/x[::-1]
        """

        #g1 = g_l(ell1,self.z1)
        #g2 = g_l(ell2,self.z2)
        
        _r12 = alpha*r12
        _r13 = alpha*r13
        mat = np.conj((self.c_mn*_r13**(-1j*self.eta_n) * self.g2).T * _r12**(-1j*self.eta_m) * self.g1).T
        
        Fy1y2 = np.sum(mat) *np.pi / 16. / (_r13**self.nu2 * _r12**self.nu1)
        
        return Fy1y2.real/(self.N1 * self.N2)

    def two_sph_bessel_r1r2_ave(self, r12_min, r12_max, r13_min, r13_max, alpha=1):
        """
        Calculate F(y_1,y_2) = \int_0^\infty dx_1 / x_1 \int_0^\infty dx_2 / x_2 * f(x_1,x_2) * j_{\ell_1}(x_1y_1) * j_{\ell_2}(x_2y_2),
        where j_\ell is the spherical Bessel func of order ell.
        array y is set as y[:] = 1/x[::-1]
        """
        
        r12 = (func1(r12_max, self.nu1, self.eta_n, alpha) + 1j*func2(r12_max, self.nu1, self.eta_m, alpha))-\
              (func1(r12_min, self.nu1, self.eta_n, alpha) + 1j*func2(r12_min, self.nu1, self.eta_m, alpha))
        r12 /= alpha**3*(r12_max**3-r12_min**3)/3
        
        r13 = (func1(r13_max, self.nu2, self.eta_n, alpha)+1j*func2(r13_max, self.nu2, self.eta_m, alpha))-\
              (func1(r13_min, self.nu2, self.eta_n, alpha)+1j*func2(r13_min, self.nu2, self.eta_m, alpha))
        r13 /= alpha**3*(r13_max**3-r13_min**3)/3

        #mat = np.conj( ((self.c_mn* r13 * self.g2).T * r12 * self.g1)).T
        mat = np.dot(r12 * self.g1, self.c_mn* r13 * self.g2)

        Fy1y2 = np.sum(mat) *np.pi / 16.
        
        return Fy1y2.real/(self.N1 * self.N2)
    
    def two_sph_bessel_r1r2_bis(self, r12, r13, alpha=1):
        """
        Calculate F(y_1,y_2) = \int_0^\infty dx_1 / x_1 \int_0^\infty dx_2 / x_2 * f(x_1,x_2) * j_{\ell_1}(x_1y_1) * j_{\ell_2}(x_2y_2),
        where j_\ell is the spherical Bessel func of order ell.
        array y is set as y[:] = 1/x[::-1]
        """
        
        #mat = np.conj( ((self.c_mn* r13 * self.g2).T * r12 * self.g1)).T
        mat = np.dot(r12 * self.g1, self.c_mn* r13 * self.g2)

        Fy1y2 = np.sum(mat) *np.pi / 16.
        
        return Fy1y2.real/(self.N1 * self.N2)

    def two_sph_bessel(self, ell1, ell2):
        """
        Calculate F(y_1,y_2) = \int_0^\infty dx_1 / x_1 \int_0^\infty dx_2 / x_2 * f(x_1,x_2) * j_{\ell_1}(x_1y_1) * j_{\ell_2}(x_2y_2),
        where j_\ell is the spherical Bessel func of order ell.
        array y is set as y[:] = 1/x[::-1]
        """

        g1 = g_l(ell1,self.z1)
        g2 = g_l(ell2,self.z2)
        
        mat = np.conj((self.c_mn*(self.x20*self.y20)**(-1j*self.eta_n) * g2).T * (self.x10*self.y10)**(-1j*self.eta_m) * g1).T
        mat_right = mat[:,self.N2//2:]
        mat_adjust = np.vstack((mat_right[self.N1//2:,:],mat_right[1:self.N1//2,:]))
        # print(mat_adjust[0][1])
        Fy1y2 = ((irfft2(mat_adjust) *np.pi / 16./ self.y2**self.nu2).T / self.y1**self.nu1).T
        # print(Fy1y2)
        return self.y1[self.N_extrap_high:self.N1-self.N_extrap_low], self.y2[self.N_extrap_high:self.N2-self.N_extrap_low], Fy1y2[self.N_extrap_high:self.N1-self.N_extrap_low, self.N_extrap_high:self.N2-self.N_extrap_low]

### Utility functions ####################

## functions related to gamma functions
def g_m_vals(mu,q):
    '''
    g_m_vals function is adapted from FAST-PT
    '''
    imag_q= np.imag(q)
    
    g_m=np.zeros(q.size, dtype=complex)

    cut =200
    asym_q=q[np.absolute(imag_q) >cut]
    asym_plus=(mu+1+asym_q)/2.
    asym_minus=(mu+1-asym_q)/2.
    
    q_good=q[ (np.absolute(imag_q) <=cut) & (q!=mu + 1 + 0.0j)]

    alpha_plus=(mu+1+q_good)/2.
    alpha_minus=(mu+1-q_good)/2.
    
    g_m[(np.absolute(imag_q) <=cut) & (q!= mu + 1 + 0.0j)] =gammafunc(alpha_plus)/gammafunc(alpha_minus)

    # asymptotic form                                 
    g_m[np.absolute(imag_q)>cut] = np.exp( (asym_plus-0.5)*np.log(asym_plus) - (asym_minus-0.5)*np.log(asym_minus) - asym_q \
        +1./12 *(1./asym_plus - 1./asym_minus) +1./360.*(1./asym_minus**3 - 1./asym_plus**3) +1./1260*(1./asym_plus**5 - 1./asym_minus**5) )

    g_m[np.where(q==mu+1+0.0j)[0]] = 0.+0.0j
    
    return g_m

def g_l(l,z_array):
    '''
    gl = 2.**z_array * gamma((l+z_array)/2.) / gamma((3.+l-z_array)/2.)
    '''
    gl = 2.**z_array * g_m_vals(l+0.5,z_array-1.5)
    return gl

def g_l_smooth(l,z_array, binwidth_dlny, alpha_pow):
    '''
    gl_smooth = 2.**z_array * gamma((l+z_array)/2.) / gamma((3.+l-z_array)/2.) * exp((alpha_pow - z_array)*binwidth_dlny -1. ) / (alpha_pow - z_array)
    '''
    gl = 2.**z_array * g_m_vals(l+0.5,z_array-1.5)
    gl *= (np.exp((alpha_pow - z_array)*binwidth_dlny) -1. ) / (alpha_pow - z_array)
    return gl

## Window function
def c_window(n,n_cut):

    n_right = n[-1] - n_cut
    n_left = n[0]+ n_cut 

    n_r=n[ n[:]  > n_right ] 
    n_l=n[ n[:]  <  n_left ] 
    
    theta_right=(n[-1]-n_r)/float(n[-1]-n_right-1) 
    theta_left=(n_l - n[0])/float(n_left-n[0]-1) 

    W=np.ones(n.size)
    W[n[:] > n_right]= theta_right - 1/(2*np.pi)*np.sin(2*np.pi*theta_right)
    W[n[:] < n_left]= theta_left - 1/(2*np.pi)*np.sin(2*np.pi*theta_left)
    
    return W


## Extrapolation
def log_extrap(x, N_extrap_low, N_extrap_high):

    low_x = high_x = []
    if(N_extrap_low):
        dlnx_low = np.log(x[1]/x[0])
        low_x = x[0] * np.exp(dlnx_low * np.arange(-N_extrap_low, 0) )
    if(N_extrap_high):
        dlnx_high= np.log(x[-1]/x[-2])
        high_x = x[-1] * np.exp(dlnx_high * np.arange(1, N_extrap_high+1) )
    x_extrap = np.hstack((low_x, x, high_x))
    return x_extrap

def bilinear_extra_P(fk1k2, N_low, N_high):
    '''
    2d bilinear extrapolation of the input fk1k2 matrix

    fk1k2: input matrix
    N_low: number of points to extrapolate on the lower sides
    N_high: number of points to extrapolate on the higher sides
    '''
    logfk1k2 = np.log(fk1k2) # This Extrapolation only works in log space
    h_grad_left = logfk1k2[:,1]-logfk1k2[:,0] # horizontal gradient left side
    h_grad_right= logfk1k2[:,-1]-logfk1k2[:,-2] # horizontal gradient right side
    add_left = np.arange(-N_low,0)
    left_matrix = np.matrix(h_grad_left).T.dot(np.matrix(add_left)) + np.matrix(logfk1k2[:,0]).T
    add_right = np.arange(1,N_high+1)
    right_matrix= np.matrix(h_grad_right).T.dot(np.matrix(add_right)) + np.matrix(logfk1k2[:,-1]).T
    new_logfk1k2 = np.hstack((left_matrix,logfk1k2,right_matrix)) ## type: matrix

    v_grad_up = new_logfk1k2[1,:] - new_logfk1k2[0,:] ## type: matrix
    v_grad_down=new_logfk1k2[-1,:]- new_logfk1k2[-2,:] ## type: matrix
    up_matrix = np.matrix(add_left).T.dot(v_grad_up) + np.matrix(new_logfk1k2[0,:])
    down_matrix=np.matrix(add_right).T.dot(v_grad_down)+np.matrix(new_logfk1k2[-1,:])
    result_matrix= np.vstack((up_matrix,new_logfk1k2,down_matrix)) ## type: matrix
    return np.exp(np.array(result_matrix))## type: array

from astropy.io import fits

class Model3PCF_FFTlog:
    
    def __init__(self, file_bk, nk=512, nOrders=11):
        self.zeta_cov = None
        self.fact = None
        self.nOrders = 0
        self.nu1 = None
        self.eta_m = None
        self.nu2 = None
        self.eta_n = None
        self.read_model(file_bk, nk, nOrders)
        self.flatten = lambda t: [item for sublist in t for item in sublist]
        
        self.bound_conf = None
        self.bound_table = None
        
    def read_model(self, file_bk, nk=512, nOrders=11):
        self.nOrders = nOrders
        bk_terms = np.array([ fits.open(file_bk%i)[1].data["L%d"%j].reshape(nk, nk) for i in [0, 3, 6] for j in range(nOrders) ])
        K1_2 = fits.open(file_bk%0)[1].data["k1"].reshape(nk, nk)
        K2_2 = fits.open(file_bk%0)[1].data["k2"].reshape(nk, nk)
        k1 = np.unique(K1_2)
        factor = K1_2**3 * K2_2**3 / (2 * np.pi**2)**2
        self.zeta_conv = [two_sph_bessel(k1, k1, factor*bk, N_pad=50) for bk in bk_terms]
        _ = [self.zeta_conv[i*nOrders+ell].set_gg(ell, ell) for i in range(3) for ell in range(nOrders)]
        self.fact = np.array([(1j)**(2*ell) for i in range(3) for ell in range(nOrders)]).real    
        self.nu1 = self.zeta_conv[0].nu1
        self.eta_m = self.zeta_conv[0].eta_m        
        self.nu2 = self.zeta_conv[0].nu2
        self.eta_n = self.zeta_conv[0].eta_n
        
    def get_terms(self, b1, gamma, gamma_t):
        return b1**3 * np.array([1, gamma, gamma_t])
    
    def bind(self, conf, alpha=1, binSize=10):
        self.bound_conf = conf
        print (self.get_single_conf_tables(conf.r12, conf.r13, alpha, binSize).shape)
        print(np.array(conf.leg_pols).T.shape)
        self.bound_table = np.dot(self.get_single_conf_tables(conf.r12, conf.r13, alpha, binSize),\
                                  np.array(conf.leg_pols).T)
    
    def get_single_conf_tables(self, r12, r13, alpha=1, binSize=10):
        if (binSize>0):
            r12_min, r12_max = r12-0.5*binSize, r12+0.5*binSize
            r13_min, r13_max = r13-0.5*binSize, r13+0.5*binSize
            #rrs = np.array([r12_min, r12_max, r13_min, r13_max])
            
            r12 = (func1(r12_max, self.nu1, self.eta_n, alpha) + 1j*func2(r12_max, self.nu1, self.eta_m, alpha))-\
              (func1(r12_min, self.nu1, self.eta_n, alpha) + 1j*func2(r12_min, self.nu1, self.eta_m, alpha))
            r12 /= alpha**3*(r12_max**3-r12_min**3)/3
        
            r13 = (func1(r13_max, self.nu2, self.eta_n, alpha)+1j*func2(r13_max, self.nu2, self.eta_m, alpha))-\
                  (func1(r13_min, self.nu2, self.eta_n, alpha)+1j*func2(r13_min, self.nu2, self.eta_m, alpha))
            r13 /= alpha**3*(r13_max**3-r13_min**3)/3
            
            return np.array([self.fact[i]*zz.two_sph_bessel_r1r2_bis(r12, r13, alpha)\
                             for i,zz in enumerate(self.zeta_conv)]).reshape(3, self.nOrders)   
        else:
            return np.array([self.fact[i]*zz.two_sph_bessel_r1r2(r12, r13, alpha)\
                             for i,zz in enumerate(self.zeta_conv)]).reshape(3, self.nOrders)   
    
    def get_bound_model(self, b1, gamma, gamma_t):
        return np.dot(self.get_terms(b1, gamma, gamma_t), self.bound_table)
                      #np.dot(self.get_terms(b1, gamma, gamma_t), self.bound_table))
    
    def get_single_conf_model(self, r12, r13, b1, gamma, gamma_t, alpha=1, binSize=10):
        return np.dot(self.get_terms(b1, gamma, gamma_t),\
                      self.get_single_conf_tables(r12, r13, alpha, binSize))
    
    def get_multiple_conf_model(self, configurations, b1, gamma, gamma_t, alpha=1, binSize=10):
        return [self.get_single_conf_model(cc.r12, cc.r13, b1, gamma, gamma_t, alpha, binSize)\
                 for i, cc in enumerate(configurations) if cc in configurations]
    
    def __call__(self, configurations, b1, gamma, gamma_t, alpha=1, binSize=10):
        model_by_conf = self.get_multiple_conf_model(configurations.values(), b1, gamma, gamma_t, alpha=alpha, binSize=binSize)
        model = [np.dot(cc.leg_pols, model_by_conf[i]) for i, cc in enumerate(configurations.values())]
    
        return np.array(self.flatten(model))
    
class GalaxyClustering:

    def __init__(self, kk, table_Matter, table_NW, table_bias, table_TNS, sigma8_z0, nMu=5, losc=110, ks=0.2):

        self.sigma8_z0 = 0
        self.mu = None
        self.weights = None
        self.kk = None
        self.MU0  = None
        self.MU   = None
        self.MU2  = None
        self.leg2 = None
        self.leg2 = None
        self.MU4  = None
        self.KMU2 = None
        
        self.keys_pk = ["lin", "dd22", "dd13", "dt22", "dt13", "tt22", "tt13"]
        self.pk_nw = {}
        self.pk_nw_interp = {}
        self.pk_lin = {}
        self.pk_lin_interp = {}
        
        self.keys_bias = ["b2d", "bs2d", "b22", "b2bs2", "bs22", "b2t", "bs2t", "b3nl"]
        self.pk_bias = {}
        self.pk_bias_interp = {}

        self.keys_TNS = ["A11", "A12", "A22", "A23", "A33"]
        self.keys_TNS += ["B12", "B13", "B14", "B22", "B23", "B24", "B33", "B34", "B44"]
        self.pk_TNS = {}
        self.pk_TNS_interp = {}
        
        self.sigmaNL2 = None
        self.deltaSigma = None
        self.set_internal(kk, nMu, table_Matter, table_NW, table_bias, table_TNS, sigma8_z0, losc, ks)

    def set_pk_tables(self, table_Matter, table_NW, table_bias, table_TNS, sigma8_z0):
        self.sigma8_z0 = sigma8_z0

        kk, pklin, pkdd22, pkdd13, pkdt22, pkdt13, pktt22, pktt13 = np.genfromtxt(
            table_Matter, unpack=True)
        
        for key in self.keys_pk:
            self.pk_lin_interp[key] = interp1d(kk, locals()["pk%s"%key])
        
        kk, pknwlin, pknwdd22, pknwdd13, pknwdt22, pknwdt13, pknwtt22, pknwtt13 = np.genfromtxt(
            table_NW, unpack=True)
        
        for key in self.keys_pk:
            self.pk_nw_interp[key] = interp1d(kk, locals()["pknw%s"%key])
        
        kk, pkb2d, pkbs2d, pkb22, pkb2bs2, pkbs22, pkb2t, pkbs2t, s3sq, const = np.genfromtxt(
            table_bias, unpack=True)
        pkb3nl = s3sq * self.pk_nw_interp["lin"](kk)
        
        for key in self.keys_bias:
            self.pk_bias_interp[key] =  interp1d(kk, locals()["pk%s"%key], "cubic")
            
        kk, A11, A12, A22, A23, A33 = np.genfromtxt(table_TNS[0], unpack=True)
        kk, B12, B13, B14, B22, B23, B24, B33, B34, B44 = np.genfromtxt(table_TNS[1], unpack=True)
        for key in self.keys_TNS:
            self.pk_TNS_interp[key] = interp1d(kk, locals()[key], "cubic")
        

    def set_internal(self, kk, nMu, table_Matter, table_NW, table_bias, table_TNS, sigma8_z0, losc=110, ks=0.2):

        self.set_pk_tables(table_Matter, table_NW, table_bias, table_TNS, sigma8_z0)

        #self.mu, self.weights = np.polynomial.legendre.leggauss(nMu)
        self.mu = np.linspace(0., 1, int(2**nMu)+1)
        self.dMu = self.mu[1]-self.mu[0]
        self.kk = kk
        X, Y = np.meshgrid(self.kk, self.mu)
        
        self.K = X
        self.K2 = X**2
        self.MU0 = Y**0
        self.MU = Y
        self.MU2 = Y**2
        self.leg2 = 0.5 * (3 * self.MU2 - 1)

        self.MU4 = Y**4
        self.leg4 = 0.125 * (35 * self.MU4 -30 * self.MU2 + 3)
        
        self.MU6 = Y**6
        self.leg6 = 1./16 * (231 * self.MU6 -315*self.MU4 + 105 * self.MU2 - 5)

        self.MU8 = Y**8
        
        self.KMU2 = X**2 * Y**2
        
        self.w1_0 = cbl.Legendre_polynomial_mu_average(0., 0.5, 0)
        self.w1_2 = cbl.Legendre_polynomial_mu_average(0., 0.5, 2)
        self.w1_4 = cbl.Legendre_polynomial_mu_average(0., 0.5, 4)
        self.w1_6 = cbl.Legendre_polynomial_mu_average(0., 0.5, 6)

        self.w2_0 = cbl.Legendre_polynomial_mu_average(0.5, 1, 0)
        self.w2_2 = cbl.Legendre_polynomial_mu_average(0.5, 1, 2)
        self.w2_4 = cbl.Legendre_polynomial_mu_average(0.5, 1, 4)
        self.w2_6 = cbl.Legendre_polynomial_mu_average(0.5, 1, 6)

        self.interpolate_pk_terms(X, Y)
        self.Sigma2 = self.get_sigma2(losc=losc, ks=ks)
        self.deltaSigma2 = self.get_deltaSigma2(losc=losc, ks=ks)
        self.Sigma2_tot_func = lambda ff : self.Sigma2+self.MU2*\
                      (2*ff*self.Sigma2+ff**2*(self.Sigma2-self.deltaSigma2))+\
                      self.MU4*ff**2*self.deltaSigma2
        
    def get_sigma2(self, losc=110, ks=0.2):
        
        integrand = lambda q : self.pk_nw_interp["lin"](q) * (1-cbl.j0(q*losc)+2*cbl.j2(q*losc))
        
        return quad(integrand, self.kk[0], ks, limit=50000, epsabs=0, epsrel=1.e-5)[0]*1/(6*np.pi**2)
        
    def get_deltaSigma2(self, losc=110, ks=0.2):
        
        integrand = lambda q : self.pk_nw_interp["lin"](q) * cbl.j2(q*losc)
        
        return quad(integrand, self.kk[0], ks, limit=50000, epsabs=0, epsrel=1.e-5)[0]/(2*np.pi**2)
     
    def interpolate_pk_terms(self, kk, mu):
        
        for key in self.keys_pk:
            self.pk_lin[key] = self.pk_lin_interp[key](kk)
            self.pk_nw[key] = self.pk_nw_interp[key](kk)
        
        for key in self.keys_bias:
            self.pk_bias[key] = self.pk_bias_interp[key](kk)

        for key in self.keys_TNS:
            self.pk_TNS[key] = self.pk_TNS_interp[key](kk) 
        
        MU2 = mu**2
        MU4 = mu**4
        MU6 = mu**6
        MU8 = mu**8
        
        self.TNS_terms = [MU2 * self.pk_TNS["A11"],
                          MU2 * self.pk_TNS["A12"],
                          MU4 * self.pk_TNS["A22"],
                          MU4 * self.pk_TNS["A23"],
                          MU6 * self.pk_TNS["A33"],
                          MU2 * self.pk_TNS["B12"],
                          MU2 * self.pk_TNS["B13"],
                          MU2 * self.pk_TNS["B14"],
                          MU4 * self.pk_TNS["B22"],
                          MU4 * self.pk_TNS["B23"],
                          MU4 * self.pk_TNS["B24"],
                          MU6 * self.pk_TNS["B33"],
                          MU6 * self.pk_TNS["B34"],
                          MU8 * self.pk_TNS["B44"]]
        self.TNS_terms = np.array(self.TNS_terms)
    
    def pk_matter_IR_resum(self, sigma8, Sigma2, nonlin=1, kind="dd"):

        norm1 = (sigma8/self.sigma8_z0)**2
        norm2 = nonlin*norm1**2
        Sigma2 *= norm1
        
        pk_nw = self.pk_nw["lin"]*norm1
        pk_nw_22 = self.pk_nw["%s22"%kind]*norm2
        pk_nw_13 = self.pk_nw["%s13"%kind]*norm2

        pk_lin = self.pk_lin["lin"]*norm1
        pk_lin_22 = self.pk_lin["%s22"%kind]*norm2
        pk_lin_13 = self.pk_lin["%s13"%kind]*norm2
        
        pk_nw_1loop = pk_nw_22+pk_nw_13
        pk_lin_1loop = pk_lin_22+pk_lin_13
        
        pk_w = pk_lin - pk_nw        
        pk_w_1loop = pk_lin_1loop - pk_nw_1loop
        
        k2S  = self.K2 * Sigma2
        return pk_nw + pk_nw_1loop + np.exp(-k2S) * pk_w *\
               (1+k2S) + np.exp(-k2S) * ( pk_w_1loop)
    
    def get_TNS_terms(self, ff, sigma8):
        norm = (sigma8/self.sigma8_z0)**4
        arr = np.array([ff, ff**2, ff**2, ff**3, ff**3,\
                        ff**2, ff**3, ff**4, ff**2, ff**3,\
                        ff**4, ff**3, ff**4, ff**4])
        return norm * np.sum([arr[i]*self.TNS_terms[i] for i in range(len(arr))], axis=0)
    
    def pk_polar(self, b1, b2, bs2, b3nl, ff, s8, sigmav, sigmaz=0, nonlin=1, bias2=1, useTNS=1):
        Sigma2 = self.Sigma2_tot_func(ff)
        pk_dd = self.pk_matter_IR_resum(s8, Sigma2, nonlin, "dd")
        pk_dt = self.pk_matter_IR_resum(s8, Sigma2, nonlin, "dt")
        pk_tt = self.pk_matter_IR_resum(s8, Sigma2, nonlin, "tt")
        
        ApB = useTNS * self.get_TNS_terms(ff, s8)
        
        norm2 = nonlin*(s8/self.sigma8_z0)**4
        pk_gg = b1**2 * pk_dd + bias2*norm2*(
            2*b1*b2 * self.pk_bias["b2d"] + 2*b1*bs2 * self.pk_bias["bs2d"] +
            b2*b2 * self.pk_bias["b22"] + 2*b2*bs2*self.pk_bias["b2bs2"] + 
            bs2*bs2 * self.pk_bias["bs22"] + 2 * b1 * b3nl * self.pk_bias["b3nl"])

        pk_gt = b1 * pk_dt + bias2*norm2*(
            b2 * self.pk_bias["b2t"] + bs2 * self.pk_bias["bs2t"] + b3nl * self.pk_bias["b3nl"])

        damping = np.power(1 + self.KMU2 * sigmav**2 * ff**2, -1)*np.exp(-self.KMU2 * sigmaz**2)

        return (pk_gg*self.MU0 + 2.*ff*self.MU2*pk_gt + ff*ff*self.MU4*pk_tt + ApB)*damping

    def pk_multipoles(self, b1, b2, bs2, b3nl, ff, s8, sigmav, sigmaz, nonlin=1, bias2=1, useTNS=1):
        polar = self.pk_polar(b1, b2, bs2, b3nl, ff, s8, sigmav, sigmaz, nonlin=nonlin, bias2=bias2, useTNS=useTNS)
        return romb(polar, self.dMu, axis=0),\
               5*romb(self.leg2*polar, self.dMu, axis=0),\
               9*romb(self.leg4*polar, self.dMu, axis=0)

    def xi_multipoles(self, rad, b1, b2, bs2, b3nl, ff, s8, sigmav, sigmaz, nonlin=1, bias2=1, useTNS=1, fact=1):
        polar = self.pk_polar(b1, b2, bs2, b3nl, ff, s8, sigmav, sigmaz, nonlin=nonlin, bias2=bias2, useTNS=useTNS)
        pk0 = romb(polar, self.dMu, axis=0)
        pk2 = 5*romb(self.leg2*polar, self.dMu, axis=0)
        return np.array(cbl.transform_FFTlog(rad, 1, self.kk, pk0*np.exp(-fact*self.kk**2), 0)),\
            -np.array(cbl.transform_FFTlog(rad, 1, self.kk, pk2*np.exp(-fact*self.kk**2), 2))
        
    def xi_wedges(self, rad, b1, b2, bs2, b3nl, ff, s8, sigmav, sigmaz, nonlin=1, bias2=1, useTNS=1, fact=1):
        polar = self.pk_polar(b1, b2, bs2, b3nl, ff, s8, sigmav, sigmaz, nonlin=nonlin, bias2=bias2, useTNS=useTNS)
        pk0 = romb(polar, self.dMu, axis=0)
        pk2 = 5*romb(self.leg2*polar, self.dMu, axis=0)
        pk4 = 9*romb(self.leg4*polar, self.dMu, axis=0)
        pk6 = 13*romb(self.leg6*polar, self.dMu, axis=0)

        xi0 = np.array(cbl.transform_FFTlog(rad, 1, self.kk, pk0*np.exp(-fact*self.kk**2), 0))
        xi2 = -np.array(cbl.transform_FFTlog(rad, 1, self.kk, pk2*np.exp(-fact*self.kk**2), 2))
        xi4 = np.array(cbl.transform_FFTlog(rad, 1, self.kk, pk4*np.exp(-fact*self.kk**2), 4))
        xi6 = -np.array(cbl.transform_FFTlog(rad, 1, self.kk, pk6*np.exp(-fact*self.kk**2), 6))

        return xi0 * self.w1_0 + xi2 * self.w1_2 + xi4 * self.w1_4 + xi6 * self.w1_6,\
               xi0 * self.w2_0 + xi2 * self.w2_2 + xi4 * self.w2_4 + xi6 * self.w2_6
    
class XiModelAP:
    
    def __init__ (self, rad, interp_xi0, interp_xi2, interp_xi4, nMu = 5):
        self.rad = rad
        self.nMu = 2**nMu + 1
        self.mu = np.linspace(0., 1., self.nMu)

        self.dMu = self.mu[1]-self.mu[0]

        self.R, self.Mu = np.meshgrid(self.rad, self.mu)
        self.Mu2 = self.Mu**2
        self.P2 = 0.5 * (3*self.Mu2 - 1)
        self.P4 = 0.125 * (35 * self.Mu**4 -30 * self.Mu2 + 3)
        
        self.set_interp(interp_xi0, interp_xi2, interp_xi4)
        
    def set_interp(self, interp_xi0, interp_xi2, interp_xi4):
        self.interp_xi0 = interp_xi0
        self.interp_xi2 = interp_xi2
        self.interp_xi4 = interp_xi4
    
    def polar(self, rad, mu):
        xi0, xi2, xi4 = self.interp_xi0(rad), self.interp_xi2(rad), self.interp_xi4(rad)
        P2 = 0.5 * (3*mu**2 - 1)
        P4 = 0.125 * (35 * mu**4 - 30 * mu**2 + 3)
        return xi0 + xi2*P2 + xi4*P4
    
    def polar_AP (self, alpha_perp, alpha_par):
        fact = np.sqrt(alpha_par**2 * self.Mu2 + alpha_perp**2 * (1-self.Mu2))
        rad = self.R * fact
        mu  = alpha_par * self.Mu / fact
        
        return self.polar(rad, mu)
    
    def multipoles(self, alpha_perp, alpha_par):
        polar = self.polar_AP(alpha_perp, alpha_par)
        xi0 = romb(polar, self.dMu, axis=0)
        xi2 = 5*romb(polar*self.P2, self.dMu, axis=0)
        xi4 = 9*romb(polar*self.P4, self.dMu, axis=0)
        return xi0, xi2, xi4
    
    def wedges(self, alpha_perp, alpha_par):
        xi0, xi2, xi4 = self.multipoles(alpha_perp, alpha_par)
        return xi0 - 3./8 * xi2 + 15./128 * xi4,\
               xi0 + 3./8 * xi2 - 15./128 * xi4
    
import glob
import torch
from torch import nn
from torch.utils.data import DataLoader, Dataset
from torchvision import datasets
from torchvision.transforms import ToTensor, Lambda, Compose
import matplotlib.pyplot as plt

class ZetaAPDataset(Dataset):
    def __init__(self, directory, transform=True, target_transform=True):
        files = glob.glob("%s/zeta*"%directory)
        data = np.array([np.genfromtxt(ff, dtype=np.float32) for ff in files])
        
        index = np.arange(len(data))
        self.alpha = data[:, 0]
        self.models = np.reshape(data[:, 1:], newshape=(len(files), 20, 11))

        self.transform = transform
        self.target_transform = target_transform
        self.transform_func = Lambda( lambda a : torch.tensor([[a]]))
        self.target_transform_func = Lambda( lambda a : torch.tensor(a) )

    def __len__(self):
        return len(self.models)

    def __getitem__(self, idx):
        alpha = self.alpha[idx]
        model = self.models[idx]
        if self.transform:
            alpha = self.transform_func(alpha)
        if self.target_transform:
            model = self.target_transform_func(model).flatten()
        return alpha, model
    
    def get(self, idx):
        alpha = self.alpha[idx]
        model = self.models[idx]
        return alpha, model
